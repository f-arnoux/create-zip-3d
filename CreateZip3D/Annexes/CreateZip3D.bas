'#Reference {50A7E9B0-70EF-11D1-B75A-00A0C90564FE}#1.0#0#C:\Windows\SysWOW64\shell32.dll#Microsoft Shell Controls And Automation
'#Reference {420B2830-E718-11CF-893D-00A0C9054228}#1.0#0#C:\Windows\SysWOW64\scrrun.dll#Microsoft Scripting Runtime
'#uses "..\..\Libraries\winapi32.bas"
'#uses "..\..\Libraries\defutils.bas"
'#uses "..\..\Libraries\defineAph.bas"
'#uses "..\..\Libraries\act3Dglobal.bas"
'#uses "..\..\Libraries\defineA.bas"
'#uses "..\..\Libraries\defineActker.bas"
'#uses "..\..\Libraries\defineAtgker.bas"
'#uses "..\..\basic\act3D\actConst.bas"
'#uses "..\..\basic\act3D\actIhmConst.bas"
'#uses "..\..\basic\act3D\ihmApi.bas"
'#uses "..\..\basic\act3D\actUtil.bas"
'#uses "..\..\basic\act3D\actUtil2.bas"
'#uses "..\..\basic\act3D\actAutoTask.bas"
'#uses "..\..\basic\act3D\actAutoUpdateProgram.bas"
'#uses "..\..\basic\act3D\actSaveFile.bas"
'#uses "..\..\basic\utils\Archi_auto.bas"

'#uses "..\..\Libraries\defutils.bas"
'#uses "..\..\basic\utils\Util.bas"
'#uses "..\..\basic\utils\dtd.bas"

Declare Function APH_getAlmaId Lib "aph.dll" () As Integer

Option Explicit

Private Const ZIP_FILE_LABEL As String = "Faire un zip"


' MHI constants
Private Const ID_CREATE_SUPPORT_ZIP_FILE As Long = 200101


Private Const IsSensible As Integer = 0

Dim oShell As Shell
Dim oFSO As Scripting.FileSystemObject

Sub DefineCreateSupportZipAction()
	' define the zip filter
	Dim param As IRscParameter
	Dim rsc As IResource
	Dim mainMenu As IRscMenu
	Dim moduleItem As IRscMenu
	Dim filePopup As IRscMenu
	Dim ActCut3DDoc As ActCut3D.Document
	Dim actItem As IRscMenu
	Dim act As IRscAction
	Dim bitmap As String

	Set ActCut3DDoc = ActCut3DApp.ActiveDocument
	Set rsc = ActCut3DDoc.Resource
	Set mainMenu = rsc.GetMenu()
	Set moduleItem = mainMenu.Find(IDR_MENU_MODULE)
	Set filePopup = moduleItem.Find(IDR_MENU_FILE)

	If Not filePopup Is Nothing Then
		bitmap = APH_getAppPath_() + "basic\utils\createZip.bmp"
		Set actItem = filePopup.AddItem("Faire un zip", "Faire un zip", bitmap)

		Set act = rsc.AddAction("AID_Action")
		Call act.SetModule("AID")

		'aFilter= " (*.zip)|*.zip"
		'Set param = rsc.CreateGlobalStringParam(ID_CREATE_SUPPORT_ZIP, ZIP_FILE_LABEL)
	    'param.GetParam.StringValue = aFilter

		Call act.ConnectToMenu(actItem)
		Call act.DoOnExecute("Call ExecuteCreateSupportZip()")
	End If
End Sub

Sub ExecuteCreateSupportZip()
	Dim ActCut3DDoc As ActCut3D.Document
    Set ActCut3DDoc = ActCut3DApp.ActiveDocument

	Dim fileName As String
	Dim zipName As String
	Dim RepSource As String
	Dim RscFolderName As String
	Dim newFilePath As String
	Dim SupportDir As String
	Dim tmpFilePath As String

	Dim ClientNumFile As Integer
    Dim ClientNameFile As String

    'Call MsgBox(AppTitle)
	Set oFSO = CreateObject("Scripting.FileSystemObject")
	Set oShell = CreateObject("Shell.Application")

	fileName = ActCut3DDoc.Resource.GetGlobalParam(ID_FILE_OPTIONS_FILE).Value

	'get zip path with a windows explorer
	SupportDir = APH_getKeyPath_("ZipFiles")
    zipName = GetFilePath(fileName,"zip",APH_getContextPath_() & SupportDir,"Create a zip", 3)
    If Len(zipName) < 1 Then Exit Sub

	' open progress bar

	ClientNumFile= FreeFile
	ClientNameFile="Alma"
	If ExistFile(APH_getAppPath_()+"none.txt") Then
		Open APH_getAppPath_()+"none.txt" For Input As #ClientNumFile
		While EOF(ClientNumFile)<>-1
			Line Input #ClientNumFile,ClientNameFile
		Wend
		If ClientNameFile="" Then
			ClientNameFile="Alma"
		End If
		Close  #ClientNumFile
	End If

	zipName = Left(zipName,Len(zipName)-Len(Split(zipName,"\")(UBound(Split(zipName,"\"))))) & ClientNameFile & "_Actcut" & GetProductVersion (APH_getBinPath_(), "ainfo.dll") & "_" & Split(zipName,"\")(UBound(Split(zipName,"\")))
	RepSource = Left(zipName,Len(zipName)-4)
	zipName = RepSource & ".zip"
	Call AppBeginAction(3, "Save zip " + zipName)
  	'Call MsgBox(RepSource & ".zip")
	'Copy rsc path
	'RscFolderName = Split(APH_getRscPath_(),"\")(UBound(Split(APH_getRscPath_(),"\"))-1)
  	'If oFSO.FolderExists(APH_getRscPath_()) Then
  	'	Call ExecCmd ("xcopy.exe " & Chr(34) &  APH_getRscPath_() & Chr(34) & " " & Chr(34) & RepSource & "\" & RscFolderName & Chr(34) & " /E /R /Y /I /C")
  	'End If
  	'Name (RepSource & "\" & RscFolderName) As RepSource & "\" & "Rsc"
  	'Create Rsc zip
	'supportZipper(RepSource & "\Rsc.zip")
	'RmDir(RepSource)
	'Call ExecCmd("RmDir " & RepSource & "\Rsc" & "\ /s /q")
	If Not existDirectory(RepSource) Then
		MkDir RepSource
	End If
	If Not existDirectory(RepSource & "\Files") Then
		MkDir RepSource & "\Files"
	End If
  	'Copy machine path
  	'If oFSO.FolderExists(APH_getContextPath_() & "machines") Then
  	'	Call ExecCmd ("xcopy.exe " & Chr(34) &  APH_getContextPath_() & "machines" & Chr(34) & " " & Chr(34) & RepSource & "\Files\machines" & Chr(34) & " /E /R /Y /I /C")
  	'End If
  	'save current file
  	'Call MsgBox(RepSource & "\Files\" & fileName & ".act")
  	Call AppStepAction()
	Call saveActFile(RepSource & "\Files\" & fileName & ".act")
	Call AppStepAction()
	'Get screen capture
	'If ExistFile(APH_getBinPath_() & "utils\screenshot\screencapture.exe") Then
	'	Call FileCopy (APH_getBinPath_() & "utils\screenshot\screencapture.exe",RepSource & "\screencapture.exe")
	'	Call ExecCmd (RepSource & "\screencapture.exe " & Chr(34) &  RepSource & "\Files\capture.png" & Chr(34) & " ActWeld")
	'	Call Kill(RepSource & "\screencapture.exe")
	'End If

	'get last log files
	'Call CopyLastLogFiles(RepSource & "\Files")

	'Create file zip
	'supportZipper(RepSource & "\Files.zip")
	'RmDir(RepSource)
	'Call ExecCmd("RmDir " & RepSource & "\Files" & "\ /s /q")

	Call GenerateMachine_txt(ContextList(),RepSource & "\Files", IsSensible)

	'Create support dir zip
	'supportZipper(zipName)
	'RmDir(RepSource)
	'Call ExecCmd("RmDir " & RepSource & "\ /s /q")

	'Call MsgBox("La cr�ation du r�pertoire " & Split(zipName,"\")(UBound(Split(zipName,"\"))) & " est termin�e")
	Call AppEndAction()
	'Call MsgBox(APH_getBinPath_ & "CreateZip3D.exe " & RepSource & " " & AppTitle)
	Call ExecCmd (APH_getBinPath_ & "CreateZip3D.exe " & RepSource & " " & AppTitle)

	'Shell("explorer.exe , """ + APH_getContextPath_() & SupportDir + "",vbNormalFocus)
	' close progress bar

End Sub

Sub CopyLastLogFiles(RepSource As String)
	Dim LogFolderName As String, logFolder As Object, logFile As Object
	Dim Tabl1() As String, Tabl2() As Double, Result() As String
	Dim i As Long, j As Long, ii As Long, posLog As Long, nbLog As Long

	LogFolderName =	APH_getAppPath_() + "log\"

	'Get log file names and date
	Set logFolder = oFSO.GetFolder(LogFolderName)
	With logFolder
	    ReDim Tabl1(logFolder.Files.Count - 1)
	    ReDim Tabl2(logFolder.Files.Count - 1)
	    i = -1
	    For Each logFile In logFolder.Files
            i = i + 1
            Tabl1(i) = logFile.Name
            Tabl2(i) = CDbl(logFile.DateLastModified)
	    Next logFile
	End With

	'sort files by date
	nbLog = UBound(Tabl2)
	ReDim Result(nbLog)
    For i = 0 To nbLog
        posLog = 0
        For j = 0 To nbLog
            If Tabl2(i) > Tabl2(j) And i <> j Then
                posLog = posLog + 1
            End If
        Next
        For ii = 1 To 1
            If Result(nbLog - posLog) = "" Then
                Result(nbLog - posLog) = Tabl1(i)
            Else
                posLog = posLog + 1
                ii = ii - 1
            End If
        Next
    Next

    'Copy the 3 last files
    MkDir(RepSource & "\log")
    For i = 0 To 2
        FileCopy(LogFolderName & Result(i), RepSource & "\log\" & Result(i))
    Next i
End Sub

Sub supportZipper(zipName As String)
	Debug.Print("Zip")

	Const ForReading = 1, ForWriting = 2, ForAppending = 8

	Dim Source, Destination, MyHex, MyBinary, i
	Dim oFolder, oCTF

	Source = Split(zipName, ".")(0)
	Destination = zipName


	'Creation de l'ent�te du zip
	MyHex =	Array(80, 75, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

	For i = 0 To UBound(MyHex)
	    MyBinary = MyBinary & Chr(MyHex(i))
	Next

	'Creation du zip
	Set oCTF = oFSO.CreateTextFile(Destination, True)
	oCTF.Write MyBinary
	oCTF.Close
	Set oCTF = Nothing

	'remplissage du zip
	Set oFolder = oShell.NameSpace(Source)

	If Not oFolder Is Nothing Then oShell.NameSpace(Destination).CopyHere oFolder.Items

End Sub
