﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateZip3D
{
    class Utils
    {
        public static void Copy(string sourceDirectory, string targetDirectory)
        {
            var diSource = new DirectoryInfo(sourceDirectory);
            var diTarget = new DirectoryInfo(targetDirectory);

            CopyAll(diSource, diTarget);
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }

        public static void DeleteAll(string pathToDelete)
        {
            DirectoryInfo source = new DirectoryInfo(pathToDelete);
            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                Console.WriteLine(@"Copying {0}", fi.Name);
                fi.Delete();
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DeleteAll(diSourceSubDir.FullName);
            }
            source.Delete();
        }

        public static List<FileInfo> Sort(string path, string filter)
        {
            FileInfo[] fi = new DirectoryInfo(path).GetFiles(filter);
            List<FileInfo> fis = new List<FileInfo>();
            foreach (FileInfo file in fi) fis.Add(file);
            fis.Sort(new ModificationDateComparer());
            return fis;
        }

        private class ModificationDateComparer : IComparer<FileInfo>
        {
            public int Compare(FileInfo f1, FileInfo f2)
            {
                return DateTime.Compare(f1.LastWriteTime, f2.LastWriteTime);
            }

        }
    }
}
