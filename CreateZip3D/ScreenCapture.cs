﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// reference  
// https://gallery.technet.microsoft.com/scriptcenter/eeff544a-f690-4f6b-a586-11eea6fc5eb8

using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace CreateZip3D
{
    /// Provides functions to capture the entire screen, or a particular window, and save it to a file. 

    public class ScreenCapture
    {
        private enum ProcessDPIAwareness
        {
            ProcessDPIUnaware = 0,
            ProcessSystemDPIAware = 1,
            ProcessPerMonitorDPIAware = 2
        }

        [DllImport("shcore.dll")]
        private static extern int SetProcessDpiAwareness(ProcessDPIAwareness value);

        [StructLayout(LayoutKind.Sequential)]
        public struct Rect
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [DllImport("user32.dll")]
        private static extern int SetForegroundWindow(IntPtr hWnd);

        private const int SW_RESTORE = 9;

        [DllImport("user32.dll")]
        private static extern IntPtr ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);
        public static Bitmap CaptureApplication(string procName)
        {
            Process proc;

            // Cater for cases when the process can't be located.
            try
            {
                proc = Process.GetProcessesByName(procName)[0];
            }
            catch (IndexOutOfRangeException e)
            {
                return null;
            }

            //Debugger.Launch();
            // You need to focus on the application
            SetForegroundWindow(proc.MainWindowHandle);
            //ShowWindow(proc.MainWindowHandle, SW_RESTORE);

            // You need some amount of delay, but 1 second may be overkill
            Thread.Sleep(300);

            SetDpiAwareness();

            Rect rect = new Rect();
            IntPtr error = GetWindowRect(proc.MainWindowHandle, ref rect);

            // sometimes it gives error.
            while (error == (IntPtr)0)
            {
                error = GetWindowRect(proc.MainWindowHandle, ref rect);
            }
            Screen screen = Screen.FromHandle(proc.MainWindowHandle);
            Screen primaryScreen = Screen.PrimaryScreen;
            Screen secondScreen = Screen.AllScreens[0];

            var screenWidth = rect.right - rect.left;
            var screenHeight = rect.bottom - rect.top;
            var screenTop = rect.top;
            var screenLeft = rect.left;

            /*using (StreamWriter sw = new StreamWriter("c:\\temp\\rect.txt", false))
            {
                sw.WriteLine("primaryScreenTop " + primaryScreen.Bounds.Location.Y);
                sw.WriteLine("primaryScreenLeft " + primaryScreen.Bounds.Location.X);
                sw.WriteLine("primaryScreenWidth " + primaryScreen.Bounds.Width);
                sw.WriteLine("primaryScreenHeight " + primaryScreen.Bounds.Height);
                sw.WriteLine("secondScreenTop " + secondScreen.Bounds.Location.Y);
                sw.WriteLine("secondScreenLeft " + secondScreen.Bounds.Location.X);
                sw.WriteLine("secondScreenWidth " + secondScreen.Bounds.Width);
                sw.WriteLine("secondScreenHeight " + secondScreen.Bounds.Height);
                sw.WriteLine("screenTop " + screen.Bounds.Location.Y);
                sw.WriteLine("screenLeft " + screen.Bounds.Location.X);
                sw.WriteLine("screenWidth " + screen.Bounds.Width);
                sw.WriteLine("screenHeight " + screen.Bounds.Height);
                sw.WriteLine("rect.top " + rect.top);
                sw.WriteLine("rect.left " + rect.left);
                sw.WriteLine("rect.bottom " + rect.bottom);
                sw.WriteLine("rect.right " + rect.right);
            }*/

            var pt1 = new Point(screenLeft, screenTop);
            var pt2 = Point.Empty;
            Bitmap bmp = new Bitmap(screenWidth, screenHeight);
            Graphics.FromImage(bmp).CopyFromScreen(pt1, pt2, new Size(screenWidth, screenHeight), CopyPixelOperation.SourceCopy);

            return bmp;
        }
        public static void SetDpiAwareness()
        {
            try
            {
                if (Environment.OSVersion.Version.Major >= 6)
                {
                    SetProcessDpiAwareness(ProcessDPIAwareness.ProcessPerMonitorDPIAware);
                }
            }
            catch (EntryPointNotFoundException)//this exception occures if OS does not implement this API, just ignore it.
            {
            }
        }
    }
}
