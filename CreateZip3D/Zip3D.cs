﻿using Actcut;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using Microsoft.VisualBasic;
using System.IO;
using System.Windows.Forms;
using System.Diagnostics;

namespace CreateZip3D
{
    class Zip3D
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Demarrage de l'appli, nb args = " + args.Count().ToString());
            //MessageBox.Show("Demarrage de l'appli, nb args = " + args.Count().ToString());
            if (args.Count() == 2)
            {
                string zipPath = args[0];//"D:\\Alma\\data\\Support_client\\Support\\Alma_Actcut3.10.5.135178_1810";//"D:\\Alma\\data\\Support_client\\Support\\Alma_Actcut3.10.5.134527_Alma_Actcut3.10.5.134527_formation"; 
                string applicationName = args[1];//"ActWeld";//
                string zipFilesPath = zipPath + "\\Files";
                string zipRscPath = zipPath + "\\Rsc";
                //MessageBox.Show("1");
                string actPath = DefineAph.GetKeyPath("act_path");
                //MessageBox.Show("2");
                string rscfolder = DefineAph.GetRscPath();
                //MessageBox.Show("3");
                string contextfolder = DefineAph.GetContextPath();
                //MessageBox.Show("4");
                string setupfolder = DefineAph.GetAppPath();

                Console.WriteLine("ZipPath : " + zipPath);
                //MessageBox.Show("ZipPath : " + zipPath);
                Console.WriteLine("Application name : " + applicationName);
                //MessageBox.Show("Application name : " + applicationName);
                Console.WriteLine("Rsc path : " + rscfolder);
                //MessageBox.Show("Rsc path : " + rscfolder);
                //var splt = appPath.Split(';');

                if (Directory.Exists(zipFilesPath))
                {
                    foreach (string path in actPath.Split(';'))
                    {
                        if (path.Contains(contextfolder) && Directory.Exists(path))
                        {
                            Utils.Copy(path, Path.Combine(zipFilesPath, path.Split('\\').Last()));
                            //MessageBox.Show("Copy : " + Path.Combine(zipFilesPath, path.Split('\\').Last()));
                        }
                    }


                    if (Directory.Exists(setupfolder + "log"))
                    {
                        CopyLastLogFiles(Path.Combine(setupfolder, "log"), "*.*", Path.Combine(zipFilesPath, "log"), 5);

                        //MessageBox.Show("Copy log files ");
                    }
                    //Debugger.Launch();

                    if (File.Exists(Path.Combine(zipFilesPath, "capture.jpg"))) File.Delete(Path.Combine(zipFilesPath, "capture.jpg"));
                    using (Bitmap bitmap = ScreenCapture.CaptureApplication(applicationName))
                    {
                        //MessageBox.Show("Capture ");
                        if (bitmap != null) bitmap.Save(Path.Combine(zipFilesPath, "capture.jpg"), ImageFormat.Jpeg);
                    }

                    GetRegister.GetTechnoReg(contextfolder, Path.Combine(zipFilesPath, "techno.reg"));
                    if (File.Exists(zipFilesPath + ".zip")) File.Delete(zipFilesPath + ".zip");
                    ZipFile.CreateFromDirectory(zipFilesPath, zipFilesPath + ".zip");
                    Utils.DeleteAll(zipFilesPath);
                }
                //else MessageBox.Show("File path don't exist");
                if (Directory.Exists(rscfolder))
                {
                    //Debugger.Launch();
                    if (File.Exists(zipRscPath + ".zip")) File.Delete(zipRscPath + ".zip");
                    Utils.Copy(rscfolder, zipRscPath);
                    CleanRsc(zipRscPath);
                    ZipFile.CreateFromDirectory(zipRscPath, zipRscPath + ".zip");
                    Utils.DeleteAll(zipRscPath);
                }
                //else MessageBox.Show("Rsc path don't exist");
                if (File.Exists(zipPath + ".zip")) File.Delete(zipPath + ".zip");
                ZipFile.CreateFromDirectory(zipPath, zipPath + ".zip");
                Utils.DeleteAll(zipPath);
                Console.WriteLine("Zip file created");
                //MessageBox.Show("Zip file created");
            }
        }

        public static void CopyLastLogFiles(string logPath, string filter, string copyPath, int nbFiles)
        {
            Directory.CreateDirectory(copyPath);
            List<FileInfo> fis = Utils.Sort(logPath, filter);
            for (int i = fis.Count - nbFiles; i < fis.Count; i++)
            {
                if (File.Exists(Path.Combine(copyPath, fis[i].Name))) File.Delete(Path.Combine(copyPath, fis[i].Name));
                fis[i].CopyTo(Path.Combine(copyPath, fis[i].Name));
            }
        }

        public static void CleanRsc(string rscPath)
        {
            if (Directory.Exists(Path.Combine(rscPath)))
            {
                DirectoryInfo rsc = new DirectoryInfo(rscPath);
                foreach (FileInfo rscfile in rsc.GetFiles())
                    if (rscfile.Extension == ".zip" && rscfile.Name.Contains("Backup"))
                        rscfile.Delete();
            }
            if (Directory.Exists(Path.Combine(rscPath,"cell_init")))
            {
                DirectoryInfo cell_init = new DirectoryInfo(Path.Combine(rscPath, "cell_init"));
                foreach (FileInfo rscfile in cell_init.GetFiles())
                    if (rscfile.Extension == ".001"
                        || rscfile.Extension == ".002"
                        || rscfile.Extension == ".003"
                        || rscfile.Extension == ".004"
                        || rscfile.Extension == ".005"
                        || rscfile.Extension == ".bak"
                        || rscfile.Extension == ".app"
                        || rscfile.Extension == ".rle"
                        || rscfile.Extension == ".log") rscfile.Delete();
            }
        }


    }
}
