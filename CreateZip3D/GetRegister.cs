﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace CreateZip3D
{
    class GetRegister
    {
        public static void GetTechnoReg(string contextfolder, string regfile)
        {
            //Debugger.Launch();
            string almaLocalMachineReg = "HKEY_LOCAL_MACHINE\\SOFTWARE\\WOW6432Node\\Alma\\ActCut-3.10";
            string ContextPathKey = "SetupContextPath";
            string contextPath = (string)Registry.GetValue(almaLocalMachineReg, ContextPathKey, string.Empty);
            string almaTechnoCurrentUserReg = "HKEY_CURRENT_USER\\SOFTWARE\\Alma\\ActCut-3.10\\Explorer\\Technologies\\Technologie";
            string TechnologieKey = "TechnoContext";
            string TechnologieContextPath = string.Empty;
            bool technoFound = false;
            int i = 0;
            do
            {
                i++;
                TechnologieContextPath = (string)Registry.GetValue(almaTechnoCurrentUserReg + i, TechnologieKey, string.Empty);
                if (TechnologieContextPath != null)
                {
                    TechnologieContextPath = TechnologieContextPath.Replace("<ACTCUT_SETUP_CONTEXT>", "");
                    if (Path.Equals(Path.GetDirectoryName(contextfolder),
                                    Path.GetDirectoryName(Path.Combine(contextPath, TechnologieContextPath))))
                        technoFound = true;
                }
            } while (TechnologieContextPath != null && technoFound == false);
            if (technoFound)
            {
                exportRegistry(almaTechnoCurrentUserReg + i, regfile);
            }
        }

        static void exportRegistry(string strKey, string filepath)
        {
            try
            {
                using (Process proc = new Process())
                {
                    proc.StartInfo.FileName = "reg.exe";
                    proc.StartInfo.UseShellExecute = false;
                    proc.StartInfo.RedirectStandardOutput = true;
                    proc.StartInfo.RedirectStandardError = true;
                    proc.StartInfo.CreateNoWindow = true;
                    proc.StartInfo.Arguments = "export \"" + strKey + "\" \"" + filepath + "\" /y";
                    proc.Start();
                    string stdout = proc.StandardOutput.ReadToEnd();
                    string stderr = proc.StandardError.ReadToEnd();
                    proc.WaitForExit();
                }
            }
            catch (Exception ex)
            {
                // handle exception
            }
        }
    }
}
